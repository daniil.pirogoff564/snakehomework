// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_SnakeElement_generated_h
#error "SnakeElement.generated.h already included, missing '#pragma once' in SnakeElement.h"
#endif
#define SNAKEGAME_SnakeElement_generated_h

#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execSetFirstElementType) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetFirstElementType_Implementation(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetFirstElementType) \
	{ \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->SetFirstElementType_Implementation(); \
		P_NATIVE_END; \
	}


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_EVENT_PARMS
#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_CALLBACK_WRAPPERS
#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASnakeElement(); \
	friend struct Z_Construct_UClass_ASnakeElement_Statics; \
public: \
	DECLARE_CLASS(ASnakeElement, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElement)


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_INCLASS \
private: \
	static void StaticRegisterNativesASnakeElement(); \
	friend struct Z_Construct_UClass_ASnakeElement_Statics; \
public: \
	DECLARE_CLASS(ASnakeElement, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(ASnakeElement)


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASnakeElement(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASnakeElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElement(ASnakeElement&&); \
	NO_API ASnakeElement(const ASnakeElement&); \
public:


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASnakeElement(ASnakeElement&&); \
	NO_API ASnakeElement(const ASnakeElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASnakeElement); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASnakeElement); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASnakeElement)


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_SnakeElement_h_11_PROLOG \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_EVENT_PARMS


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_CALLBACK_WRAPPERS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_INCLASS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_SnakeElement_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_CALLBACK_WRAPPERS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_SnakeElement_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class ASnakeElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_SnakeElement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
